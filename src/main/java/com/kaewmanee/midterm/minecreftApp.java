package com.kaewmanee.midterm;

import java.util.Scanner;

public class minecreftApp { 
    static Zombie zombie = new Zombie("bob",100,10,0); 
    static Skeleton bone  = new Skeleton("bone",90,10,0);
    static Hero hero = new Hero("hero",200,10,0);
    static Scanner sc = new Scanner(System.in);
    public static String input(){
        return sc.next();
    }
    public static void command(){
        System.out.println("----Command----");
        System.out.println("  az : attack zombie");
        System.out.println("  ab : attack bone");
        System.out.println("  ah : attack hero");
        System.out.println("  hz : recover zombie");
        System.out.println("  hb : recover bone");
        System.out.println("  hh : recover hero");
        System.out.println("  sh : hero call sword (attack +10)");
        System.out.println("  sz : zombie call sword (attack +10)");
        System.out.println("  sb : bone call arrow (attack +10)");
        System.out.println("  z_ar : zombie get armor(blood +100)");
        System.out.println("  h_ar : hero get armor(blood +100)");
        System.out.println("  b_ar : bone get armor(blood +100)");
        System.out.println("  q  : Quit game");
        System.out.println("----------------");
        System.out.println();
    }
    public static void process(String command){
        switch (command) {
            
            case "az":
                zombie.heroHited();
                System.out.println("+-----------------+");
                System.out.println("| Attack Zombie ! |");
                System.out.println("+-----------------+");
                break;
            case "ab":
                bone.heroHited();
                System.out.println("+-----------------+");
                System.out.println("| Attack Skeleton!|");
                System.out.println("+-----------------+");
                break;
            case "ah":
                hero.skeletonHited();
                hero.zombieHited();
                System.out.println("+--------------------------------+");
                System.out.println("| Zombie and Skeleton Attack You!|"); 
                System.out.println("+--------------------------------+");
                break;
            case "hz":
                zombie.recover();
                System.out.println("+------------------+");
                System.out.println("| Recover Zombie ! |");
                System.out.println("+------------------+");
                break;
            case "hb":
                bone.recover();
                System.out.println("+---------------------+");
                System.out.println("|  Recover Skeleton ! |");
                System.out.println("+---------------------+");
                break;
            case "hh":
                hero.recover();
                System.out.println("+-------------------+");
                System.out.println("|  + Recover Hero + |");
                System.out.println("+-------------------+");
                break;
            case "sh":
                hero.callSword();
                System.out.println("+--------------------+");
                System.out.println("| + Hero get Sword + |");
                System.out.println("+--------------------+");
                break;
            case "sz":
                zombie.callSword();
                System.out.println("+----------------------+");
                System.out.println("| + Zombie get Sword + |");
                System.out.println("+----------------------+");
                break;
            case "sb":
                bone.callSword();
                System.out.println("+---------------------+");
                System.out.println("| Skeleton get Sword  |");
                System.out.println("+---------------------+");
                break;
            case "z_ar":
                zombie.getArmor();
                System.out.println("+--------------------+");
                System.out.println("|  Zombie get Armor  |");
                System.out.println("+--------------------+");
                break;
            case "h_ar":
                hero.getArmor();
                System.out.println("+--------------------+");
                System.out.println("|  Hero get Armor    |");
                System.out.println("+--------------------+");
                break;
            case "b_ar":
                bone.getArmor();
                System.out.println("+----------------------+");
                System.out.println("|  Skeleton get Armor  |");
                System.out.println("+----------------------+");
                break;
            case "q":
                System.out.println("+-----------------------------+");
                System.out.println("| Exit Game have a good grade |");
                System.out.println("+-----------------------------+");
                System.exit(0);
            case "command":
                command();
            default:
                break;
        }
    }
    public static void main( String[] args )
    {
        hero.battleWithSkeleton(bone);
        hero.battleWithZombie(zombie);
        zombie.battleWithHero(hero);
        bone.battleWithHero(hero);
        command();

        while(true) {
            hero.showStatus();
            System.out.println("---------------");
            zombie.showStatus();
            System.out.println("---------------");
            bone.showStatus();
            System.out.println("*input 'command' to see the command");
            System.out.println();

            if(zombie.getBlood()<=0 || bone.getBlood()<=0){
                System.out.println("+---------------------------+");
                System.out.println("|          You Win !!       |");
                System.out.println("+---------------------------+");
                System.exit(0);
            }
            if(hero.getBlood()<=0){
                System.out.println("+---------------------------+");
                System.out.println("|         You loose !!      |");
                System.out.println("+---------------------------+");
                System.exit(0);
            }

            String command = input();
            process(command);
        }
    }
}
