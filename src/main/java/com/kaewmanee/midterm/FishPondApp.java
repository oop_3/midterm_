package com.kaewmanee.midterm;

import java.util.Scanner;

public class FishPondApp {
    static Fish fish = new Fish("bob");
    static Frog frog = new Frog("fophy");
    static Salamander larva = new Salamander("larva");
    static Scanner sc = new Scanner(System.in);

    public static String input() {
        return sc.next();
    }
    public static void document(){
        System.out.println("+--------Document---------+");
        System.out.println("| fish  : feed the fish   |");
        System.out.println("| frog  : feed the frog   |");
        System.out.println("| larva : feed the larva  |");
        System.out.println("| doc   : open document   |");
        System.out.println("+--------Document---------+");
        System.out.println();
    }

    public static void process(String command) {
        switch (command) {
            case "fish":
                fish.feed();
                fish.showStatus();
                System.out.println();
                break;
            case "frog":
                frog.feed();
                frog.showStatus();
                System.out.println();
                break;
            case "larva":
                larva.feed();
                larva.showStatus();
                System.out.println();
                break;
            case"doc":
                document();
                break;
            case"q":
                System.out.println("+-----------------------+");
                System.out.println("|      Good Bye!!       |");
                System.out.println("+-----------------------+");;
                System.exit(0);
            default:
                break;
        }

    }

    public static void main(String[] args) {
        document();
        while (true) {
            String command = input();
            process(command);
            System.out.println();
        }

    }
}

