package com.kaewmanee.midterm;

public class Frog extends Animal{
    private String evo;
    public Frog(String name) {
        super(name);
    }
    public String getEvo() {
        evo();
        return evo;
    }
    public String evo() {
        if(getLevel()==1){
            evo = "Tadpole ";
        }
        if(getLevel()==2){
            evo = "Frogle";
        }
        if(getLevel()==3){
            evo = "Frog";
        }
        return evo;
    } 
    public void showStatus() {
        System.out.println("+------------------------------+");
        System.out.println("  name  : " + getName() );
        System.out.println("  point : " + getPoint() );
        System.out.println("  level : " + getLevel() );
        System.out.println("  evolution : " + getEvo());
        System.out.println("+------------------------------+");
    }
    
}

