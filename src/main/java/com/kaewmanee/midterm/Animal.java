package com.kaewmanee.midterm;

public class Animal {
    private String name;
    private int point;
    private int level;
    public static final int MAX_level = 3;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public int getPoint() {
        return point;
    }

    public int feed() {
        point += 5;
        printFeed();
        upLevel();
        return getPoint();
    }

    public int upLevel() {
        if (point < 5) {
            level = 1;
        }if (point >= 5) {
            level = 2;
        }if (point >= 11) {
            level = 3;
        }
        checkLevel();
        return getLevel();
    }

    public void checkLevel() {
        if (level == 2 && point==10) {
            printUplevel();
        }
        if (level == 3 && point == 15) {
            printUplevel();
        }
        if (level == 3 && point == 20){
            printMaxLevel();
            System.exit(0);
        }
    }

    public void printFeed() {
        System.out.println("+----------------+");
        System.out.println("| Thank for food |");
        System.out.println("+----------------+");
    }

    public void printUplevel() {
        System.out.println("+----------------+");
        System.out.println("|    Level Up    |");
        System.out.println("+----------------+");
    }
    public void printMaxLevel(){
        System.out.println("+------------------------------------------+");
        System.out.println("|  Max Level Thank you for giving me food  |");
        System.out.println("+------------------------------------------+");
    }

    public void showStatus() {
        System.out.println("+-----------------------+");
        System.out.println(" name  : " + name );
        System.out.println(" point : " + point );
        System.out.println(" level : " + level );
        System.out.println("+-----------------------+");
    }
}

