package com.kaewmanee.midterm;

public class Player {
    private String name;
    private int blood;
    private int protection;
    private int attack;
    private Zombie zombie;
    private Skeleton skeleton;
    private Hero hero;
    public static final int Max_attack = 30;
    public static final int Max_protection = 20;
    public static final int height = 2;

    public Player(String name, int blood, int attack, int protection) {
        this.name = name;
        this.blood = blood;
        this.attack = attack;
        this.protection = protection;
    }

    public Player(String name) {
        this.blood = 100;
        this.attack = 10;
        this.protection = 5;
    }

    public int getProtection() {
        return protection;
    }

    public int getBlood() {
        return blood;
    }

    public String getName() {
        return name;
    }

    public int getAttack() {
        return attack;
    }

    public int getArmor() {
        if (getProtection() < Max_protection) {
            protection += 20;
            return getProtection();
        } else {
            return getProtection();
        }

    }

    public Zombie getZombie() {
        return zombie;
    }

    public Hero getHero() {
        return hero;
    }

    public Skeleton getSkeleton() {
        return skeleton;
    }

    public int callSword() {
        if (attack >= Max_attack) {
            return getAttack();
        } else {
            attack += 10;
            return getAttack();
        }

    }

    public void showStatus() {
        System.out.println("name : " + name);
        System.out.println("power attack : " + attack);
        System.out.println("blood : " + blood);
        System.out.println("protection : " + protection);
    }

    public void battleWithZombie(Zombie zombie) {
        this.zombie = zombie;
    }

    public void battleWithHero(Hero hero) {
        this.hero = hero;
    }

    public void battleWithSkeleton(Skeleton skeleton) {
        this.skeleton = skeleton;

    }

    public int recover() {
        blood += 5;
        return blood;
    }

    public int skeletonHited() {
        if (getProtection() <= 0) {
            blood -= skeleton.getAttack();
            return blood;
        }
        if (getProtection() > 0) {
            protection -= skeleton.getAttack();
            return protection;
        }
        return blood;

    }

    public int zombieHited() {
        if (getProtection() <= 0) {
            blood -= zombie.getAttack();
            return blood;
        }
        if (getProtection() > 0) {
            protection -= zombie.getAttack();
            return protection;
        }
        return blood;

    }

    public int heroHited() {
        if (getProtection() <= 0) {
            blood -= hero.getAttack();
            return blood;
        }
        if (getProtection() > 0) {
            protection -= hero.getAttack();
            return protection;
        }
        return blood;

    }
}
