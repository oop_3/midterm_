package com.kaewmanee.midterm;

public class Fish extends Animal{
    private String evo;
    public Fish(String name) {
        super(name);
    }
    public String evo() {
        if(getLevel()==1){
            evo = "Tiny Fish";
        }
        if(getLevel()==2){
            evo = "Just Fish";
        }
        if(getLevel()==3){
            evo = "Super Fish !!!!";
        }
        return evo;
    } 
    public void showStatus() {
        System.out.println("+------------------------------+");
        System.out.println("  name  : " + getName() );
        System.out.println("  point : " + getPoint() );
        System.out.println("  level : " + getLevel() );
        System.out.println("  evolution : " + getEvo());
        System.out.println("+------------------------------+");
    }
    public String getEvo() {
        evo();
        return evo;
    }
    
}

